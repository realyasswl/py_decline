import xgboost as xgb
from sklearn import linear_model, svm, tree
from sklearn.neural_network import MLPClassifier


class XGB():
    def __init__(self, param):
        self.bst = None
        self.param = param

    def fit(self, train_x, train_y):
        self.bst = xgb.train(self.param.items(), xgb.DMatrix(train_x, label=train_y))

    def predict(self, test_x):
        return self.bst.predict(xgb.DMatrix(test_x))


def xgb_mining(train_x, train_y, test_x, test_y):
    param = {"silent": True, "eta": 0.33, "gamma": 50}
    # param = {"silent": True, "max_depth": 30, "min_child_weight": 5, "eta": 0.3, "gamma": 2}
    dtrain = xgb.DMatrix(train_x, label=train_y)
    bst = xgb.train(param.items(), dtrain)
    test_Y = bst.predict(xgb.DMatrix(test_x))
    return result(test_x, test_y, test_Y)


def linear_mining(train_x, train_y, test_x, test_y):
    reg = linear_model.LinearRegression()
    reg.fit(train_x, train_y)
    test_Y = reg.predict(test_x)
    return result(test_x, test_y, test_Y)


def lasso_mining(train_x, train_y, test_x, test_y):
    reg = linear_model.Lasso(alpha=0.15)
    reg.fit(train_x, train_y)
    test_Y = reg.predict(test_x)
    return result(test_x, test_y, test_Y)


def lassolars_mining(train_x, train_y, test_x, test_y):
    reg = linear_model.LassoLars(alpha=.15)
    reg.fit(train_x, train_y)
    test_Y = reg.predict(test_x)
    return result(test_x, test_y, test_Y)


def nn_mining(train_x, train_y, test_x, test_y):
    clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(20, 10), random_state=1, max_iter=1000)
    clf.fit(train_x, train_y)
    test_Y = clf.predict(test_x)
    return result(test_x, test_y, test_Y)


def svm_mining(train_x, train_y, test_x, test_y):
    clf = svm.SVC()
    clf.fit(train_x, train_y)
    test_Y = clf.predict(test_x)
    return result(test_x, test_y, test_Y)


def svr_mining(train_x, train_y, test_x, test_y):
    clf = svm.SVR(C=1.0, epsilon=0.2)
    clf.fit(train_x, train_y)
    test_Y = clf.predict(test_x)
    return result(test_x, test_y, test_Y)


def result(test_x, test_y, test_Y):
    result = [abs(test_y[i] - test_Y[i]) for i in range(0, len(test_x))]
    corr = 0
    for b in result:
        corr += 1 if b <= 5 else 0
    return corr / len(test_x), result


def mine(clf, train_x, train_y, test_x, test_y):
    clf.fit(train_x, train_y)
    test_Y = clf.predict(test_x)
    return result(test_x, test_y, test_Y)
