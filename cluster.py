import main as m
from main import SkipRule, classify
import math, collections
import numpy as np

clst = m.Setting("C:\\Users\\Yonghong\\Desktop\\decline\\data.csv",
                 ["WORK_DATE", "HEAT_ID", "GETTO_TEMP", "SL1_TEMP", "SL2_TEMP", "SL2_TEMP2", "SL2_TEMP3", "BOF_ANGLE",
                  "LADLE_ID", "STEEL_SHIFT", "KONG_LU_TIME_BOF", "PRED_RH_BEGIN_TEMP", "OFRECAST_RH_BEGIN_TEMP",
                  "IF_TSO_AFTER_BLOW", "TSO_ZH", "CA_ID", "LADLE_HEAT_TURNOVER"],
                 skip_rules=[SkipRule("AR_COUNT", (lambda x: x < 0))
                     , SkipRule("TSO_END_TEMP", (lambda x: x == 0))
                     , SkipRule("BOF_END_O", (lambda x: x == 0))
                     , SkipRule("WJ", (lambda x: x > 200 or x < 20))
                     , SkipRule("LADLE_TEMP", (lambda x: x == 0))
                     , SkipRule("CAST_SEAT_TIME", (lambda x: x < 30 or x > 120))
                     , SkipRule("TG_CG_TIME", (lambda x: x <= 0 or x > 600))
                     , SkipRule("PANGTONG_TIME", (lambda x: x < 0 or x > 100))
                     , SkipRule("NET_WEIGHT", (lambda x: x < 190 or x > 235))
                     , SkipRule("ROP_BOF_TIME", (lambda x: x > 25))
                     , SkipRule("BOTTOM_BLOWING_TIME", (lambda x: x > 1000))
                     , SkipRule("KONG_LU_TIME_BOF", (lambda x: x > 400))],
                 post_excludes=["ROP_TYPE"],
                 transform_rules={
                     "ROP_TYPE": lambda x: x},
                 classifier=classify
                 )

if __name__ == '__main__':
    limit = 4
    header, dataset, standardrized = m.load(clst, True, ["WJ"], limit)
    print(header)

    for label in standardrized:
        g = {}
        header_length = len(header)
        for st in standardrized[label]:
            key = "_".join(["{0:s}_{1:d}".format(header[ind], st[ind]) for ind in range(header_length - 1)])
            value = st[header_length - 1]
            try:
                g[key].append(value)
            except KeyError as error:
                g[key] = [value]

        g = collections.OrderedDict(sorted(g.items()))

        size_one = 0
        fit_count = 0
        break_count = 0
        for g_k in g:
            if len(g[g_k]) == 1:
                size_one += 1
                continue
            # print("{0:100s}{1:s}".format(",".join(["{0:d}".format(math.floor(x)) for x in g[g_k]]), g_k))
            if np.max(g[g_k]) - np.min(g[g_k]) <= 5:
                fit_count += 1
            else:
                break_count += 1

        print("[{5:s}]: {1:d}, group: {0:d}, size one group: {2:d},fit group: {3:d},break group: {4:d} ".format(
            len(g), len(standardrized[label]), size_one, fit_count, break_count, label))
