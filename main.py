import matplotlib.pyplot as plt
from functools import reduce
import csv, math, random
from numbers import Number
import numpy as np
import mining
from sklearn.externals.six import StringIO
import graphviz, pydot
from sklearn import linear_model, svm, tree
from sklearn.neural_network import MLPClassifier
from datetime import datetime

dict_path = "C:\\Users\\Yonghong\\Desktop\\decline\\pro.csv"
ca_dict = {}
with open(dict_path) as dict_file:
    dict_csv = csv.reader(dict_file, delimiter=',')

    for dict_row in dict_csv:
        ca_dict[dict_row[-1]] = dict_row[-2]

date_2018 = datetime.strptime("2018/1/1", "%Y/%m/%d").date()


class Setting():
    def __init__(self, file_path, excludes, skip_rules=None, transform_rules={},
                 default_transform_rule=(lambda x: float(x) if len(x) > 0 else 0),
                 composition_rules=[], post_excludes=[], classifier=None):
        """

        :param file_path:The file path to data set file.
        :param excludes:Excluded column.
        :param skip_rules:Rules used to skip records.
        :param transform_rules:
        :param default_transform_rule:
        :param composition_rules:
        :param post_excludes:
        """
        self.file_path = file_path
        self.excludes = excludes
        self.skip_rules = skip_rules
        self.transform_rules = transform_rules
        self.default_transform_rule = default_transform_rule
        self.composition_rules = composition_rules
        self.post_excludes = post_excludes
        self.classifier = classifier


class SkipRule():
    def __init__(self, title, rule):
        self.title = title
        self.rule = rule


def load(setting, standardize=False, standardize_skip=[], standardize_limit=100):
    """
    Event sequence:
        exclude columns - (setting.excludes)
        transform rows - (setting.transform_rules)
        skip rows depends on transformed rows - (setting.skip_rules)
        post exclude columns - (setting.post_excludes)
    :param setting:
    :param standardize: deprecated
    :return:
    """
    file_path = setting.file_path
    post_excludes = setting.post_excludes
    excludes = setting.excludes
    skip_rules = setting.skip_rules
    with open(file_path) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        z = 0
        result = {}
        header = []
        exclude_indices = []
        post_exclude_indices = []

        for row in readCSV:
            z += 1
            if z == 1:
                header = [s.strip() for s in row]
                for ex in excludes:
                    try:
                        exclude_indices.append(header.index(ex))
                    except ValueError:
                        print("no %s in header to exclude" % ex)
                        continue

                exclude_indices.sort(reverse=True)

                for index in exclude_indices:
                    header.pop(index)

                for pe in post_excludes:
                    try:
                        post_exclude_indices.append(header.index(pe))
                    except ValueError:
                        print("no %s in header post_exclude" % pe)
                        continue

                post_exclude_indices.sort(reverse=True)

                continue
            # process row
            try:
                for index in exclude_indices:
                    row.pop(index)

                # transform feature
                mapped = [setting.transform_rules.get(header[ind], setting.default_transform_rule)(cell)
                          for ind, cell in enumerate(row)]

                # fitter by original feature
                ctn = False
                for rule in skip_rules:
                    try:
                        rule_index = header.index(rule.title)
                    except ValueError as not_found:
                        continue

                    if rule.rule(mapped[rule_index]):
                        ctn = True
                        # print(rule.title, mapped[rule_index])
                        break

                if ctn:
                    continue

                # get category by examining specific feature specified by classifier
                category = setting.classifier(mapped, header)

                # add new feature
                to_add = []
                for composition in setting.composition_rules:
                    to_add.append(composition(mapped, header))
                    pass

                # remove from the post_exclude
                for index in post_exclude_indices:
                    mapped.pop(index)

                # actually add new feature
                for t in to_add:
                    # mapped.insert(0, t)
                    mapped.append(t)

                # append final record
                try:
                    result[category].append(mapped)
                except KeyError as ke:
                    result[category] = []
                    result[category].append(mapped)
            except ValueError as err:
                print(row, err)

        for index in post_exclude_indices:
            header.pop(index)

        for id in range(len(setting.composition_rules)):
            header.append("comp{0:d}".format(id))

        standardrized = {}
        if standardize:
            for label in result:
                temp = []
                t = zip(*result[label])
                column_id = -1
                for feature in t:
                    column_id += 1
                    try:
                        standardize_skip.index(header[column_id])
                        temp.append(feature)
                    except:
                        temp.append([
                            math.floor(standardize_limit * (x - np.min(feature)) / (np.max(feature) - np.min(feature)))
                            for x in feature])
                        continue
                standardrized[label] = list(zip(*temp))

        return header, result, standardrized


def sample(ds, rate=.8, flt=None, serial=False):
    """
    Split data set into training set and testing set.
    :param ds:
    :param rate:
    :param flt:
    :param serial:
    :return:
    """
    train_set = []
    test_set = []

    for index, record in enumerate(ds):
        if flt is not None and not flt(record):
            continue

        test = record[0] == 1 if serial else random.random() > rate
        if test:
            test_set.append(record)
        else:
            train_set.append(record)
    return train_set, test_set


def split_dimension(set, target_index):
    """
    Split dimension of dataset, the last column is seen as the label.
    """
    x = [i[:target_index] + i[target_index + 1:] for i in set]
    y = [i[target_index] for i in set]
    return x, y


def all_dig(record):
    for r in record:
        if not isinstance(r, Number):
            return False
    return True


def ar1(m, h):
    return (m[h.index("AR_COUNT")] + m[h.index("BOTTOM_BLOWING_COUNT")] * 3 + m[h.index("PANGTONG_TIME")]) * \
           m[h.index("TSO_END_TEMP")] / m[h.index("NET_WEIGHT")]


def transport_stage(m, h):
    rate = 0
    if m[h.index("BAG_AGE")] < 100:
        rate = 1.6
    elif m[h.index("BAG_AGE")] < 120:
        rate = 1.7
    else:
        rate = 1.8
    return (rate * (m[h.index("ROP_BOF_TIME")] + .5 * m[h.index("TAPPING_TIME")]) * (
            m[h.index("TSO_END_TEMP")] - m[h.index("LADLE_TEMP")])
            # + .5 * m[h.index("TAPPING_TIME")] * m[h.index("TSO_END_TEMP")]
            ) / m[h.index("NET_WEIGHT")]


def print_stat(results, decimals=3,
               stats=[("MAX, ", np.max), ("AVE, ", np.average), ("MIN, ", np.min), ("STD, ", np.std)]):
    for stat in stats:
        print("{0:30s}".format(stat[0]) + ", ".join(
            [("{0:15." + str(decimals) + "f}").format(np.round(stat[1](x), decimals=decimals)) for x in results]))


def classify(m, h):
    return m[h.index("ROP_TYPE")] + "_" + m[h.index("CA_ID")]
    # + "_" + layered("TAPPING_TIME", {"(7|8]": lambda x: 7 < x <= 8}, m, h)
    # + "_" + layered("ROP_BOF_TIME", {"(10|15]": lambda x: 10 < x <= 15}, m, h)
    # + "_" + layered("TSO_END_TEMP", {"(1670|1690]": lambda x: 1670 < x <= 1690}, m, h)
    # + "_" + layered("BAG_AGE", {"(0|100]": lambda x: 0 < x <= 100}, m, h)
    # + binarify(["WHITE_ASHES_COUNT", "LIMESTONE_COUNT", "PELLET_COUNT"], m, h) + "_" \


def layered(key, fs, m, h):
    for label in fs:
        if fs[label](m[h.index(key)]):
            return "%s_%s" % (key, label)

    return "%s_else" % key


def binarify(keys, m, h):
    r = []
    for key in keys:
        r.append("0" if m[h.index(key)] == 0 else "1")
    return "".join(r)


data = Setting("C:\\Users\\Yonghong\\Desktop\\decline\\data.csv",
               ["HEAT_ID", "GETTO_TEMP", "SL1_TEMP", "SL2_TEMP", "SL2_TEMP2", "SL2_TEMP3", "BOF_ANGLE",
                "LADLE_ID", "STEEL_SHIFT", "KONG_LU_TIME_BOF", "PRED_RH_BEGIN_TEMP", "OFRECAST_RH_BEGIN_TEMP",
                "IF_TSO_AFTER_BLOW", "TSO_ZH"],
               skip_rules=[SkipRule("AR_COUNT", (lambda x: x < 0))
                   , SkipRule("TSO_END_TEMP", (lambda x: x == 0))
                   , SkipRule("BOF_END_O", (lambda x: x == 0))
                   , SkipRule("WJ", (lambda x: x > 200 or x < 20))
                   , SkipRule("LADLE_TEMP", (lambda x: x == 0))
                   , SkipRule("CAST_SEAT_TIME", (lambda x: x < 30 or x > 120))
                   , SkipRule("TG_CG_TIME", (lambda x: x <= 0 or x > 600))
                   , SkipRule("PANGTONG_TIME", (lambda x: x < 0 or x > 100))
                   , SkipRule("NET_WEIGHT", (lambda x: x < 190 or x > 235))
                   , SkipRule("ROP_BOF_TIME", (lambda x: x > 25))
                   , SkipRule("BOTTOM_BLOWING_TIME", (lambda x: x > 1000))
                   , SkipRule("KONG_LU_TIME_BOF", (lambda x: x > 400))],
               transform_rules={
                   "LADLE_HEAT_TURNOVER": lambda x: 0 if x == "在线" else (
                       150 if x == "离线" else (float(x) if len(x) > 0 else 0)),
                   "WHITE_ASHES_COUNT": lambda x: 6.5 * (float(x) if len(x) > 0 else 0),
                   "LIMESTONE_COUNT": lambda x: 18 * (float(x) if len(x) > 0 else 0),
                   "PELLET_COUNT": lambda x: 24 * (float(x) if len(x) > 0 else 0),
                   "GET_TSO_TIME": lambda x: (float(x) if len(x) > 0 else 0) / 60,
                   "TG_CG_TIME": lambda x: (float(x) if len(x) > 0 else 0) / 60,
                   "ROP_TYPE": lambda x: x,
                   "WORK_DATE": lambda x: 1 if datetime.strptime(x, "%Y/%m/%d").date() > date_2018 else -1,
                   "CA_ID": lambda x: ca_dict[x]},
               composition_rules=[ar1, transport_stage],
               post_excludes=["ROP_TYPE", "CA_ID", 'BOF_END_O', 'BAG_AGE', 'LADLE_HEAT_TURNOVER',
                              'BOTTOM_BLOWING_TIME'],
               classifier=classify
               )

selected = data


def mine(ms, clf_names, header, target_key, rd=100, serial=False):
    target_index = header.index(target_key)
    feature_names = header[: target_index] + header[target_index + 1:]
    display_header = ", ".join([("{0:>15s}").format(x) for x in clf_names])
    results = [[] for i in ms]
    actual_results = [[] for i in ms]
    print("%s%s" % ("{0:30s}".format("%s(%d) %s" % (category, len(result), "HIT,")), display_header))
    if serial:
        rd = 1
    for x in range(rd):
        train_set, test_set = sample(result, rate=.8, serial=serial)
        train_x, train_y = split_dimension(train_set, target_index)
        test_x, test_y = split_dimension(test_set, target_index)
        # test_x = train_x
        # test_y = train_y
        for index, clf in enumerate(ms):
            with_in_rate, actual_result = mining.mine(clf, train_x, train_y, test_x, test_y)
            results[index].append(with_in_rate)
            ave_temp_diff = reduce(lambda x, y: x + y, actual_result) / len(test_x)
            actual_results[index].append(ave_temp_diff)
    print_stat(results, decimals=4)
    print("%s%s" % ("{0:30s}".format("%s(%d) %s" % (category, len(result), "Precision,")), display_header))
    print_stat(actual_results, decimals=2)


def plot(x, category):
    fig, axs = plt.subplots(sharey=True, tight_layout=True)
    axs.hist(x, bins=20)
    plt.title(category)
    plt.show()


if __name__ == "__main__":
    target_key = "WJ"
    # mining_names = ["xgb", "linear", "lasso", "lassolars", "svm", "svr"]
    # ms = [mining.xgb_mining, mining.linear_mining, mining.lasso_mining, mining.lassolars_mining, mining.svm_mining,
    #       mining.svr_mining]

    clfs = []
    clf_names = []
    for j in np.linspace(0.1, 0.3, 3):
        clfs.append(linear_model.Lasso(alpha=j))
        clf_names.append("lasso_{0:.3f}".format(j))

    for i in np.linspace(10, 20, 3):
        i = math.floor(i)
        clfs.append(mining.XGB({"silent": True, "eta": .34, "gamma": 50, "max_depth": i}))
        clf_names.append("xgb_dpt_{0:d}".format(i))

    header, result_map, standardized = load(selected)
    print(len(header), header)
    for category in result_map:
        result = result_map[category]
        if len(result) < 30:
            print("{0:s}({1:d})".format(category, len(result)))
            continue
        mine(ms=clfs, clf_names=clf_names, header=header, target_key=target_key, rd=100, serial=False)
